`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/07/2024 03:19:59 PM
// Design Name: 
// Module Name: i2s_square_wave
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module i2s_square_wave(
    input wire mclk, // 12.288 MHz input clock
    output reg bclk = 0, // MCLK/4
    output reg pblrc = 0, // 48 kHz (MCLK/256)
    output reg pbdat = 0 // 500 Hz (MCLK/24576)
    );

    reg [9:0] count_128 = 0;  // Counter for generating pblrc
    reg [13:0] count_12288 = 0; // Counter for generating pbdat
    reg flip = 0;

    // Clock enable signals
    wire enable_bclk = (count_128[0] == 1'b1); // Enable bclk at MCLK/2
    wire enable_pblrc = (count_128 == 127);   // Enable pblrc at MCLK/128
    wire enable_pbdat = (count_12288 == 12287); // Enable pbdat at MCLK/12288

    always @(posedge mclk) begin
        // Counter for 128
        if (count_128 == 127) begin
            count_128 <= 0;
        end else begin
            count_128 <= count_128 + 1;
        end

        // Counter for 12288
        if (count_12288 == 12287) begin
            count_12288 <= 0;
            flip <= ~flip;  // Toggle flip every 12288 cycles
        end else begin
            count_12288 <= count_12288 + 1;
        end

        // Generate bclk (MCLK/4)
        if (enable_bclk) begin
            bclk <= ~bclk;
        end

        // Generate pblrc (MCLK/128)
        if (enable_pblrc) begin
            pblrc <= ~pblrc;
        end

        if (count_128 >= 4 && count_128 < 8) begin
            pbdat <= flip;
        end else begin
            pbdat <= 0;
        end
    end
endmodule